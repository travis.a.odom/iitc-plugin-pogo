# Pokemon Go Plugin
This is a Greasemonkey/Tampermonkey plugin that works with the Ingress Intel Total Conversion plugin to add user-submitted Pokemon Go information to the Ingress Intel map.

Original source on [Reddit](https://www.reddit.com/r/pokemongo/comments/4rm9rt/faq_here_is_how_niantic_knew_about_that_local/)